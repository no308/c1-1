az network nsg rule update \
    --resource-group khaled-rg \
    --nsg-name my-nsg \
    --name AllowHTTP \
    --protocol Tcp \
    --direction Inbound \
    --priority 1001 \
    --source-address-prefix '*' \
    --source-port-range '*' \
    --destination-address-prefix '*' \
    --destination-port-range 22 \
    --access Allow

