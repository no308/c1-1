#!/bin/bash

# Définir les variables pour le nom de votre groupe de ressources et votre région Azure
resource_group_name="khaled-rg"
location="FranceCentral"

# Créer un groupe de ressources
az group create --name $resource_group_name --location $location

# Créer un réseau virtuel
vnet_name="my-vnet"
vnet_address_prefix="10.0.0.0/16"
subnet_name="my-subnet"
subnet_address_prefix="10.0.0.0/24"

az network vnet create \
    --resource-group $resource_group_name \
    --name $vnet_name \
    --address-prefix $vnet_address_prefix \
    --subnet-name $subnet_name \
    --subnet-prefix $subnet_address_prefix

# Créer un groupe de sécurité réseau
nsg_name="my-nsg"
az network nsg create --resource-group $resource_group_name --name $nsg_name

# Créer une règle de sécurité pour permettre le trafic HTTP entrant
az network nsg rule create \
    --resource-group $resource_group_name \
    --nsg-name $nsg_name \
    --name AllowHTTP \
    --protocol Tcp \
    --priority 1001 \
    --destination-port-range 80 \
    --access Allow

# Créer une carte réseau avec une adresse IP publique
nic_name="my-nic"
public_ip_name="vm-public-ip"
az network public-ip create --resource-group $resource_group_name --name $public_ip_name

az network nic create \
    --resource-group $resource_group_name \
    --name $nic_name \
    --vnet-name $vnet_name \
    --subnet $subnet_name \
    --network-security-group $nsg_name \
    --public-ip-address $public_ip_name

# Créer une machine virtuelle Ubuntu
vm_name="my-vm-v1"
vm_size="Standard_DS1_v2"
admin_username="khaled_razouk"
admin_password="1a1a1a1a1a1A"
image_publisher="Canonical"
image_offer="UbuntuServer"
image_sku="18.04-LTS"
os_disk_name="my-disck-os"

az vm create \
    --resource-group $resource_group_name \
    --name $vm_name \
    --size $vm_size \
    --nics $nic_name \
    --image $image_publisher:$image_offer:$image_sku:latest \
    --admin-username $admin_username \
    --admin-password $admin_password \
    --os-disk-name $os_disk_name

# Ouvrir le port 80 pour le trafic HTTP sur la machine virtuelle
az vm open-port --resource-group $resource_group_name --name $vm_name --port 80

# Afficher les informations de connexion de la machine virtuelle
az vm show --resource-group $resource_group_name --name $vm_name --show-details --query [publicIps] --output tsv
