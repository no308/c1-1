Create a server using azure cli
The first thing to do is to create a file with the sh extension to put the script in it in order to deploy to the cloud
To do this, you need to download the Azure CLI
Bash


`curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash `

to make sure that the download was successful and that we can use it, we can check the version with the command:
`az --version`

Using the official Microsoft website we can specify the latest version of the script that we will use
The following resources must be created to achieve the required objectives
1-	Resource group
2-	Virtual network
3-	Subnet
4-	Network security group
5-	Network interface card
6-	Public ip address 
7-	Virtual machin

The next step is to prepare the variables
# the variables for creat ressource group 

`resource_group_name="khaled-rg"`
`location="FranceCentral"`

# the variables for creating the vnet
`vnet_name="my-vnet"`
`vnet_address_prefix="10.0.0.0/16"`
`subnet_name="my-subnet"`
`subnet_address_prefix="10.0.0.0/24"`

# the variables for creating the NSG & NIC
`nsg_name="my-nsg"`
`nic_name="my-nic"`
`public_ip_name="vm-public-ip"`

# the variables for creating the VM

`vm_name="my-vm-v1"`

`vm_size="Standard_DS1_v2"`
`admin_username="khaled_razouk"`
`admin_password="1a1a1a1a1a1A"`
`image_publisher="Canonical"`
`image_offer="UbuntuServer"`
`image_sku="18.04-LTS"`
`os_disk_name="my-disck-os"`

The next step is to write the script

# Create a resource group
`az group create --name $resource_group_name --location $location`

# Create a virtual network

```
az network vnet create \
    --resource-group $resource_group_name \
    --name $vnet_name \
    --address-prefix $vnet_address_prefix \
    --subnet-name $subnet_name \
    --subnet-prefix $subnet_address_prefix
```




# Create a NSG (network security group)
`az network nsg create --resource-group $resource_group_name --name $nsg_name`

# Create a security rule to allow incoming HTTP traffic
```
az network nsg rule create \
    --resource-group $resource_group_name \
    --nsg-name $nsg_name \
    --name AllowHTTP \
    --protocol Tcp \
    --priority 1001 \
    --destination-port-range 80 \
    --access Allow
```


# Create a network card with a public IP address
`az network public-ip create --resource-group $resource_group_name --name $public_ip_name`

```
az network nic create \
    --resource-group $resource_group_name \
    --name $nic_name \
    --vnet-name $vnet_name \
    --subnet $subnet_name \
    --network-security-group $nsg_name \
    --public-ip-address $public_ip_name
```


# Create an Ubuntu virtual machine
```
az vm create \
    --resource-group $resource_group_name \
    --name $vm_name \
    --size $vm_size \
    --nics $nic_name \
    --image $image_publisher:$image_offer:$image_sku:latest \
    --admin-username $admin_username \
    --admin-password $admin_password \
    --os-disk-name $os_disk_name
```



# Open port 80 for HTTP traffic on the virtual machine
`az vm open-port --resource-group $resource_group_name --name $vm_name --port 80`

# View virtual machine connection information
`az vm show --resource-group $resource_group_name --name $vm_name --show-details --query [publicIps] --output tsv`

Then the following commands should be used to login to our Azure account and then deploy the resources

```
az login
sh < Le nom du fichier contenant le script>
```


When the script starts, it shows all created resources one by one

When the deployment of the resources to Azure is finished, we will see a result

It is necessary to check now whether the virtual machine is working or not, and for this it is better to try to download the Web Server using the following script
But before you have to open port 22 to connect with ssh in the NSG

```
az network nsg rule update \
    --resource-group khaled-rg \
    --nsg-name my-nsg \
    --name AllowHTTP \
    --protocol Tcp \
    --direction Inbound \
    --priority 1001 \
    --source-address-prefix '*' \
    --source-port-range '*' \
    --destination-address-prefix '*' \
    --destination-port-range 22 \
    --access Allow 
```
         

For the connection with ssh 
`az ssh vm --resource-group khaled-rg --vm-name my-vm-v1 --subscription <sub id>`

After that, download Nginx using the following two commands
`sudo apt update`
`sudo apt install nginx`

If we put the public IP address in the browser, the web server will appear to us
